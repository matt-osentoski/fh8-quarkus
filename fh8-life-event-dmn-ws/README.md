# fh8-life-event-dmn-ws
This is a Web Service that exposes decision (DMN) files. This service deals with
life events.  The underlying technologies are Kogito on Quarkus

## Build
```
mvnw clean install
```

## Docker Setup
### Setup the Docker plugin
```
mvnw quarkus:add-extension -Dextensions="container-image-docker"
```

### Build the Docker image (JVM)
This image will use the java JVM and will be larger than a native image

``` 
mvnw clean package -Dquarkus-profile=docker -Dquarkus.container-image.build=true
```

>(NOTE: If you get the following error running the above command in windows: `Unknown lifecycle phase ".container-image`, you may have to run the command below instead:)

```shell
mvn clean package -Dquarkus-profile=docker --define quarkus.container-image.build=true
```

### (OPTIONAL) Build a native Docker image
This will create a native binary image. This image should be lighter weight and
have a smaller memory footprint than the JVM image.

>(NOTE: Since this image doesn't use a JVM for memory management, some long-running
> apps that use a lot of memory might not perform as well as the JVM equivalent.)
```
mvnw package -Pnative -Dquarkus-profile=docker -Dquarkus.native.container-build=true -Dquarkus.container-image.build=true

- OR -
## Build the image and push to the docker registry
mvnw package -Pnative -Dquarkus-profile=docker -Dquarkus.native.container-build=true -Dquarkus.container-image.build=true -Dquarkus.container-image.push=true
```

### Push the Image to your Docker registry
``` 
mvnw clean package -Dquarkus-profile=docker -Dquarkus.container-image.push=true
```

>(NOTE: If you get the following error running the above command in windows: `Unknown lifecycle phase ".container-image`, you may have to run the command below instead:)

```shell
mvn  clean package -Dquarkus-profile=docker --define quarkus.container-image.push=true
```