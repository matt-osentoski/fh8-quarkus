# Using Helmfile to deploy fh8
Helmfile allows multiple Helm deploys with a single command. The command below will be used to 
Spinup the entire fh8 application stack

# Run the Helmfile

>(NOTE: The `--environment` option must be before `apply`)
```
helmfile --environment fh8 apply
```

# Cleanup the entire stack

```
helmfile --environment fh8 destroy
```

## Restart all pods in the fh8 namespace
During development, if you aren't incrementing helm version numbers, the pods may not
restart, depending on the change.  To restart all pods in a namespace, run the following 
command.

>(NOTE: Ideally, you want to increment Helm version numbers to manage deploys. This is
>just during very active dev cycles early in the development phase of the product)

```
kubectl -n fh8 delete pod --all
```