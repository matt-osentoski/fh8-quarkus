# Using Terraform to deploy all Helm charts
You can use Terraform's Helm Provider to deploy multiple Helm projects at one time.
This is similar to Helmfile (Not to be confused with Helm) for multiple Helm project deployment.

Provider documentation can be found below:
- [https://registry.terraform.io/providers/hashicorp/helm/latest/docs](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)
- [https://github.com/hashicorp/terraform-provider-helm](https://github.com/hashicorp/terraform-provider-helm)

## Dependencies
You should have the Terraform CLI installed for your operating system, with the `terraform` executable
set in the System Path.

[https://www.terraform.io/downloads.html](https://www.terraform.io/downloads.html)

## Build all Helm Projects using Terraform

``` 
terraform init

### This will show what will be applied
terraform plan

### Deploy the charts!
terraform apply
```

## Other commands
### Format
This command will cleanup the spaces/tabbing, etc and general format of your terraform configuration
files:

``` 
terraform fmt
```

### Validate
This command will validate your terraform files, similar to a Linter:

``` 
terraform validate
```

## Cleanup 
Run the following command to cleanup all helm resources

``` 
terraform destroy
```
