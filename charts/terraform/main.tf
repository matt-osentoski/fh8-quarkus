provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "life_event_dmn" {
  name = "fh8-life-event-dmn-ws"

  #repository = "https://charts.bitnami.com/bitnami"
  chart = "../fh8-life-event-dmn-ws"

  // Don't wait for the pods to be ready.
  wait = false
  namespace = "fh8"
  values = [
    "${file("../fh8-life-event-dmn-ws/values.yaml")}"
  ]

  //  set {
  //    name  = "example.name"
  //    value = "ExampleValue"
  //  }
}